//Copyright SoftwareByNumbers Ltd 2021

//!A simple event but intended to allow a closure over a RefCell to be registered
//! against an event.  Multiple closures can be registered against a single event.
//!
//! # Use
//!
//! The target of the closure must be wrapped in a RefCell.
//!
//! ```rust
//!    use std::cell::RefCell;
//!    use event_bus::{Inter, InterBus};
//!
//!     #[derive(Hash, Debug, Clone)]
//!     pub enum EventType {
//!         ShowThree(String),
//!     }
//!
//!     impl<'a> PartialEq for EventType {
//!         fn eq(&self, other: &EventType) -> bool {
//!             match (self, other) {
//!                 (EventType::ShowThree(_), EventType::ShowThree(_)) => true,
//!                 _ => false,
//!             }
//!         }
//!     }
//!
//!     impl<'a> Eq for EventType {}
//!
//!
//!    //The text to change
//!    let display_block1 = String::from("id=2, display=none");
//!    //Text in a RefCell
//!    let db1 = RefCell::new(display_block1);
//!    //The event
//!    let show_event1 = EventType::ShowThree(String::from("AAAA"));
//!    //The bus
//!    let mut event_bus: Inter<EventType, Box<dyn Fn(&EventType)>> = Inter::new();
//!    //The closure
//!    let show1  = Box::new(|ev: &EventType|
//!                  {match ev {
//!                     EventType::ShowThree(s) => {&db1.replace(String::from(&format!("id={}, display=block",s)));},
//!                     _                       => {}
//!                   }});
//!    //Register the closure against the event
//!    event_bus.register_listener(Box::new(show1), &show_event1);
//!    //Fire the event
//!    event_bus.fire(&show_event1);
//!
//! ```
//!
//!

use std::collections::HashMap;
use std::cell::RefCell;



///The bus trait
pub trait InterBus<'a, E: 'a + Clone, F> where F: Fn(&E), {
    /// Fire an event, will call every listener for that event in turn
    fn fire(&mut self, event: &'a E);

    /// Add a listener for an event
    fn register_listener(&mut self, f: Box<F>, event: &'a E);

    ///Get all the listeners for an event
    fn listeners(&mut self, event: &'a E) -> Option<&mut Vec<Box<F>>>;

    ///Remove all the listeners for an event
    fn remove_listeners(&mut self, event:&'a E);
}

///The bus struct
pub struct Inter<E: Clone, F> where F: Fn(&E), E: Eq + std::hash::Hash {
    listeners: HashMap<E, Vec<Box<F>>>,
}

impl<'a, E: 'a + Clone, F: 'static> Inter<E, F> where F: Fn(&E), E: Eq + std::hash::Hash {
    pub fn new() -> Inter<E, F> {
        Inter{listeners: HashMap::<E, Vec<Box<F>>>::new()}
    }
}

impl<'a, E: 'a + Clone, F> InterBus<'a, E, F> for Inter<E, F> where F: Fn(&E), E: Eq + std::hash::Hash{

    //! Fire an event, will call every listener for that event in turn
    fn fire(&mut self, event: &'a E) {
        if let Some(v) = self.listeners(&event) {
            for f in v {
                f(&event.clone());
            }
        }
    }

    fn register_listener(&mut self, f: Box<F>, event: &E) {
        let m = &mut self.listeners;
        if let Some(v) = m.get_mut(&event) {
            v.push(f);
        } else {
            let mut v = Vec::<Box<F>>::new();
            v.push(f);
            m.insert(event.clone(), v);
        }
    }

    fn listeners(&mut self, event: &E) -> Option<&mut Vec<Box<F>>> {
        if let Some(v) = self.listeners.get_mut(&event) {
            Some(v)
        } else {
            None
        }
    }

    fn remove_listeners(&mut self, event:&'a E) {
        let m = &mut self.listeners;
        if let Some(v) = m.get_mut(&event) {
            v.clear();
        }
    }
}

impl< E: Clone, F> Drop for Inter<E, F> where F: Fn(&E), E: Eq + std::hash::Hash {
    fn drop(&mut self) {
        self.listeners.clear();
    }
}


#[cfg(test)]
mod dispatch_tests {
    use super::*;

    use std::mem;

    //A struct used to deliver changes in the ChangeStruct enum and can be used as a stand alone
    //event
    #[derive(Hash, Debug, Clone)]
    pub struct ChangeStruct{
        a: String,
        b: u32,
    }

    impl<'a> PartialEq for ChangeStruct {
        fn eq(&self, other: &ChangeStruct) -> bool {
             self.a == other.a && self.b == other.b
        }
    }

    impl<'a> Eq for ChangeStruct {}

    //Sample set of events
    #[derive(Hash, Debug, Clone)]
    pub enum EventType {
        ShowThree(String),
        HideThree,
        ChangeStruct(ChangeStruct),
    }

    impl<'a> PartialEq for EventType {
        fn eq(&self, other: &EventType) -> bool {
            match (self, other) {
                (EventType::ShowThree(_), EventType::ShowThree(_)) => true,
                (EventType::HideThree, EventType::HideThree) => true,
                (EventType::ChangeStruct(_), EventType::ChangeStruct(_)) => true,
                _ => false,
            }
        }
    }

    impl<'a> Eq for EventType {}




    #[test]
    fn test_examples() {

        // A function which takes a closure as an argument and calls it.
        // <F> denotes that F is a "Generic type parameter"
        fn apply<F>(f: F) where
            // The closure takes no input and returns nothing.
            F: FnOnce() {
            // ^ TODO: Try changing this to `Fn` or `FnMut`.

            f();
        }

        // A function which takes a closure and returns an `i32`.
        fn apply_to_3<F>(f: F) -> i32 where
            // The closure takes an `i32` and returns an `i32`.
            F: Fn(i32) -> i32 {

            f(3)
        }




        let greeting = "hello";
        // A non-copy type.
        // `to_owned` creates owned data from borrowed one
        let mut farewell = "goodbye".to_owned();

        // Capture 2 variables: `greeting` by reference and
        // `farewell` by value.
        let diary = || {
            // `greeting` is by reference: requires `Fn`.
            println!("I said {}.", greeting);

            // Mutation forces `farewell` to be captured by
            // mutable reference. Now requires `FnMut`.
            farewell.push_str("!!!");
            println!("Then I screamed {}.", farewell);
            println!("Now I can sleep. zzzzz");

            // Manually calling drop forces `farewell` to
            // be captured by value. Now requires `FnOnce`.
            mem::drop(farewell);
        };

        // Call the function which applies the closure.
        apply(diary);

        // `double` satisfies `apply_to_3`'s trait bound
        let double = |x| 2 * x;

        println!("3 doubled: {}", apply_to_3(double));
    }




    #[test]
    ///Test an event with a struct event  and a struct target
    fn test_simple_struct_dispatch<'a>() {

        #[derive(Hash, Debug, Clone)]
        pub struct TestStruct {
            a: String,
            b: u32,
            c: String,
        }

        impl std::default::Default for TestStruct {
            fn default() -> TestStruct {
                TestStruct{
                    a: String::from(""),
                    b: 0,
                    c: String::from(""),
                }
            }
        }

        impl<'a> PartialEq for TestStruct {
            fn eq(&self, other: &TestStruct) -> bool {
                 self.a == other.a && self.b == other.b && self.c == other.c
            }
        }

        impl<'a> Eq for TestStruct {}

        let event1: ChangeStruct = ChangeStruct{a: String::from("Smith"), b: 222};

        let v1 = TestStruct{
            a: String::from(""),
            b: 0,
            c: String::from("Nothing"),
        };

        let rv1 = RefCell::new(v1);

        //The event bus
        let mut event_bus: Inter<ChangeStruct, Box<dyn Fn(&ChangeStruct)>> = Inter::new();

        //The closures that will be registered and then called when the above events are fired.
        let e1  = Box::new(|ev: &ChangeStruct| {let nv1 = TestStruct{a: ev.a.clone(),
                                                                     b: ev.b,
                                                                     c: String::from("Nothing"),};
                                                println!("FIRED {:?}", ev);
                                                &rv1.replace(nv1);
                                               });
        event_bus.register_listener(Box::new(e1), &event1);

        //pre conditions
        assert_eq!(&rv1.clone().take(), &TestStruct{a: String::from(""),
                                                    b: 0,
                                                    c: String::from("Nothing"),
                                                    });

        //fire the event
        event_bus.fire(&event1);

        //post conditions
        assert_eq!(&rv1.clone().take(), &TestStruct{a: String::from("Smith"), b: 222, c: String::from("Nothing")});

    }

    #[test]
    ///Test an event with a struct and a struct target
    fn test_inter_multi_struct_dispatch<'a>() {

        #[derive(Hash, Debug, Clone)]
        pub struct TestStruct {
            a: String,
            b: u32,
            c: String,
        }

        impl std::default::Default for TestStruct {
            fn default() -> TestStruct {
                TestStruct{
                    a: String::from(""),
                    b: 0,
                    c: String::from(""),
                }
            }
        }

        impl<'a> PartialEq for TestStruct {
            fn eq(&self, other: &TestStruct) -> bool {
                 self.a == other.a && self.b == other.b && self.c == other.c
            }
        }

        impl<'a> Eq for TestStruct {}

        let cs = ChangeStruct{a: String::from("Fred"), b: 32};
        let event1: EventType = EventType::ChangeStruct(cs);

        let v1 = TestStruct{
            a: String::from(""),
            b: 0,
            c: String::from("Nothing"),
        };

        let rv1 = RefCell::new(v1);

        //The event bus
        let mut event_bus: Inter<EventType, Box<dyn Fn(&EventType)>> = Inter::new();

        //The closures that will be registered and then called when the above events are fired.
        let show1  = Box::new(|ev: &EventType| {match ev {
                                                EventType::ChangeStruct(s) => {let nv1 = TestStruct{a: s.a.clone(),
                                                                                                    b: s.b,
                                                                                                    c: String::from("Nothing"),};
                                                                               &rv1.replace(nv1);},
                                                _                          => {println!("FIRED NO MATCH")}
                                                }
                                        });

       event_bus.register_listener(Box::new(show1), &event1);
       assert_eq!(&rv1.clone().take(), &TestStruct{a: String::from(""),
                                                   b: 0,
                                                   c: String::from("Nothing"),
                                               });

       //fire the event
       event_bus.fire(&event1);
       assert_eq!(&rv1.clone().take(), &TestStruct{a: String::from("Fred"), b: 32, c: String::from("Nothing")});

    }


    #[test]
    fn test_inter_multi_dispatch<'a>() {

        //The change event that causes a div to be shown with the supplied label, in this case amodel uuid
        let show_event1 = EventType::ShowThree(String::from("23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb"));
        let show_event2 = EventType::ShowThree(String::from("AAAA"));
        let hide_event1 = EventType::HideThree;

        //The values to change - mimics a div attributes for example
        let display_block1 = String::from("id=1, display=none");
        let display_block2 = String::from("id=2, display=none");

        let db1 = RefCell::new(display_block1);
        let db2 = RefCell::new(display_block2);

        //The event bus that connects the value above to the change event above
        //NB: Here the F type is a box, which means that we have to put the closure in a box in a box
        //to make the registration work!!
        //If we dont do this, we cant define the type of the event bus.
        //We cant't do
        // let mut event_bus = Inter::new();
        // because we end up trying to outlive static EVEN if we drop the bus in an inner scope!!
        //let mut event_bus: Inter<EventType, Box<dyn Fn(&EventType)>>  = Inter::new();
        let mut event_bus: Inter<EventType, Box<dyn Fn(&EventType)>> = Inter::new();

        //The closures that will be registered and then called when the above events are fired.
        let show1  = Box::new(|ev: &EventType| {match ev {
                                                    EventType::ShowThree(s) => {&db1.replace(String::from(&format!("id={}, display=block",s)));},
                                                    _                       => {}
                                                }});

        let show22  = Box::new(|ev: &EventType| {match ev {
                                                    EventType::ShowThree(s) => {&db1.replace(String::from(&format!("id={}, display=block",s)));},
                                                    _                       => {}
                                                }});


        let show2  = Box::new(|ev: &EventType| {match ev {
                                                    EventType::ShowThree(s) => {&db2.replace(String::from(&format!("id={}, display=block",s)));},
                                                    _                       => {}
                                                }});


        let hide1  = Box::new(|ev: &EventType| {match ev {
                                                    EventType::HideThree => {&db1.replace(String::from("id=1, display=none"));},
                                                    _                    => {}
                                                }});

        let hide2  = Box::new(|ev: &EventType| {match ev {
                                                    EventType::HideThree => {&db2.replace(String::from("id=2, display=none"));},
                                                    _                    => {}
                                                }});

        //register the closures against the event
        event_bus.register_listener(Box::new(show1), &show_event1);
        event_bus.register_listener(Box::new(show22), &show_event2);
        event_bus.register_listener(Box::new(show2), &show_event1);
        event_bus.register_listener(Box::new(hide1), &hide_event1);
        event_bus.register_listener(Box::new(hide2), &hide_event1);

        //preconditions
        assert_eq!(&db1.clone().take(), &String::from("id=1, display=none"));
        assert_eq!(&db2.clone().take(), &String::from("id=2, display=none"));

        //fire the show event
        event_bus.fire(&show_event1);

        //both divs should have an id and be showing (clone because RefCell is single owner)
        assert_eq!(&db1.clone().take(), &String::from("id=23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb, display=block"));
        assert_eq!(&db2.clone().take(), &String::from("id=23fed73c-0d0a-4ba0-b9a5-b7e1fd4dfcbb, display=block"));

        //fire the hide event
        event_bus.fire(&hide_event1);

        //both divs should have an id and not be showing (clone because RefCell is single owner)
        assert_eq!(&db1.clone().take(), &String::from("id=1, display=none"));
        assert_eq!(&db2.clone().take(), &String::from("id=2, display=none"));

        //check the event bus
        let l = event_bus.listeners(&show_event1);
        assert!(l.is_some());
        assert_eq!(l.unwrap().len(), 2);

        let l = event_bus.listeners(&hide_event1);
        assert!(l.is_some());
        assert_eq!(l.unwrap().len(), 2);

        //clear the hide event listeners
        event_bus.remove_listeners(&hide_event1);
        let l = event_bus.listeners(&hide_event1);
        assert!(l.is_some());
        assert_eq!(l.unwrap().len(), 0);



    }
}
