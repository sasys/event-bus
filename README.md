# Simple Event Bus

Allows a closure to close over a ref cell, be registered against an event,
and to be called on event fire.

Events must be clonable and have Hash, Partial Eq and Eq traits.

## Test

```
cargo test  -- --show-output
```
